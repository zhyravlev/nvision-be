var fs = require('fs');
var readSequelizeConfig = fs.readFileSync('./config/config.json');
var jsonSequelizeConfig = JSON.parse(readSequelizeConfig);

let config;

switch (process.env.mode) {
    default:
        config = {
            DATABASE: {
                database: jsonSequelizeConfig.development.database,
                username: jsonSequelizeConfig.development.username,
                password: jsonSequelizeConfig.development.password,
                settings: {
                    host: jsonSequelizeConfig.development.host,
                    port: jsonSequelizeConfig.development.port,
                    dialect: jsonSequelizeConfig.development.dialect,
                    pool: {
                        maxConnections: 3,
                        minConnections: 1,
                        maxIdleTime: 5000
                    },
                    timezone: 'Europe/Moscow'
                }
            }
        };
}

module.exports = config;