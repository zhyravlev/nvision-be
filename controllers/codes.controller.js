'use strict';

const Op = require('sequelize').Op;
const _ = require('lodash');
const url = require('url');
const Codes = require('../models/codes');

let codes = Object.create(null);

codes.search = async (req, res, next) => {
    try {

        let where = {};
        let search = req.query['search'];
        let exclude = JSON.parse(req.query['exclude']);

        if (search.length > 0) {
            where[Op.or] = [
                {
                    name: {
                        $iLike: `${search}%`
                    }
                },
                {
                    registration: {
                        $iLike: `${search}%`
                    }
                },
                {
                    factory: {
                        $iLike: `${search}%`
                    }
                },
                {
                    fiscalStorage: {
                        $iLike: `${search}%`
                    }
                }
            ];
        }

        if (_.isArray(exclude)) {
            _.remove(exclude, n => n === '');

            if (exclude.length > 0) {
                console.log('exclude', exclude);

                where[Op.and] = [
                    {
                        id: {
                            [Op.notIn]: exclude
                        }
                    }
                ];
            }
        }

        const searchCodes = await Codes.all({
            attributes: ['id', 'name', 'registration', 'factory', 'fiscalStorage', 'accessLevel'],
            where: where,
            order: [
                ['id', 'asc']
            ],
            limit: 5
        });

        let result = [];

        _.forEach(searchCodes, code => result.push(code.toJSON()));

        return Promise.resolve(result);
    } catch (e) {
        console.log(e);
        return Promise.reject({error: e});
    }
};

module.exports = codes;