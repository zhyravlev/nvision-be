'use strict';

const express = require('express');
const router = express.Router();

const codesCtrl = require('../controllers/codes.controller');

const badRequest = (message) => {
    const err = new Error('Bad request');
    err.status = 400;
    err.message = JSON.stringify({
        type: 'Bad request',
        code: 400,
        message: message.error
    });

    return err;
};

router.get('/', (req, res, next) => {
    codesCtrl.search(req, res, next)
        .then((response) => res.json(response))
        .catch((error) => next(badRequest(error)));
});

module.exports = router;