'use strict';

module.exports = {
    up: (queryInterface, Sequelize, done) => {
        return queryInterface.createTable('codes', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            name: {
                type: Sequelize.STRING,
                allowNull: false
            },
            registration: {
                type: Sequelize.STRING
            },
            factory: {
                type: Sequelize.STRING
            },
            fiscalStorage: {
                type: Sequelize.STRING
            },
            accessLevel: {
                type: Sequelize.ENUM('owner', 'admin', 'manager'),
                defaultValue: 'admin'
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        }).then(() => {
            for (let i = 0; i <= 1000; i++) {
                let names = ['Антон', 'Василий', 'Дмитрий', 'Гоша', 'Петя', 'Олег', 'Святослав', 'Эдуард', 'Максим'];
                let surnames = ['Иванов', 'Петров', 'Сидоров', 'Васин', 'Антонов', 'Коваленко', 'Табаксюровъ', 'Ивлиев', 'Зайцев'];
                let name = names[Math.floor(Math.random() * names.length)];
                let surname = surnames[Math.floor(Math.random() * surnames.length)];
                let fullName = `${name} ${surname}`;

                let levels = ['owner', 'admin', 'manager'];
                let accessLevel = levels[Math.floor(Math.random() * levels.length)];

                let random = () => Math.random().toString(36).substring(2, 15).toUpperCase();

                let registration = random();
                let factory = random();
                let fiscalStorage = random();

                queryInterface.sequelize.query("INSERT INTO \"codes\" (\"id\",\"name\",\"registration\",\"factory\",\"fiscalStorage\",\"accessLevel\",\"createdAt\",\"updatedAt\") VALUES (DEFAULT,'" + fullName + "','" + registration + "','" + factory + "','" + fiscalStorage + "','" + accessLevel + "',NOW(),NOW())");
            }

            done();
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('codes');
    }
};