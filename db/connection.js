const Sequelize = require('sequelize');
const config = require('../config');

const pg = require('pg');
pg.defaults.parseInt8 = true;

require('pg-parse-float')(pg);

const sequelize = new Sequelize(config.DATABASE.database, config.DATABASE.username, config.DATABASE.password, config.DATABASE.settings);

module.exports = sequelize;