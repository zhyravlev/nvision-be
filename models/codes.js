'use strict';

const connection = require('../db/connection');
const Sequelize = require('sequelize');

const Codes = connection.define('codes', {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    registration: {
        type: Sequelize.STRING
    },
    factory: {
        type: Sequelize.STRING
    },
    fiscalStorage: {
        type: Sequelize.STRING
    },
    accessLevel: {
        type: Sequelize.ENUM,
        values: ['owner', 'admin', 'manager']
    },
}, {
    freezeTableName: true
});

module.exports = Codes;